<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@create');
Route::group(['middleware' => ['api', 'auth']], function ($router) {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('refresh', 'Auth\LoginController@refresh');
    Route::get('me', 'Auth\LoginController@user');
});


Route::prefix('admin')->middleware('jwt.auth')->group(function() {
    Route::get('get-data', 'Admin\ApiController@getData');
    Route::get('polls', 'PollController@index');
    Route::get('messages', 'MessageController@index');
    Route::delete('messages/delete/{id}', 'MessageController@destroy');
    Route::post('messages/update/{id}', 'MessageController@update');
});


Route::get('admin/export-polls', 'Admin\ApiController@exportPolls');

Route::apiResource('poll', 'PollController');
Route::apiResource('message', 'MessageController');