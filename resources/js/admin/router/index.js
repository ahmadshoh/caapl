import VueRouter from "vue-router";

import Index from '../components/index';
import Polls from '../components/polls';
import Messages from '../components/messages';
import Login from '../components/login'
import Register from '../components/register'
import Foridden from '../components/forbidden'

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/admin',
            component: Index,
            name: 'AdminPanel',
            meta: {auth: ['admin', 'moderator']}
        },
        {
            path: '/admin/polls',
            name: 'Polls',
            component: Polls,
            meta: {auth: ['admin', 'moderator']}
        },
        {
            path: '/admin/messages',
            component: Messages,
            name: 'Messages',
            meta: {auth: ['admin', 'moderator']}
        },
        {
            path: '/admin/login',
            name: 'Login',
            component: Login,
            meta: {auth: false}
        },
        {
            path: '/admin/register',
            name: 'Register',
            component: Register,
            meta: {auth: false}
        },
        {
            path: '/403',
            name: 'Foridden',
            component: Foridden,
            meta: {auth: true}
        }
    ]
});