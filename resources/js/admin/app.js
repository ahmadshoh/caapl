import Vue from 'vue';
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
import App from './components/App.vue';
import VueRouter from 'vue-router';


Vue.use(VueRouter);
Vue.use(VueSweetalert2);

import 'sweetalert2/dist/sweetalert2.min.css';

import router from './router'
import Vuetify from './plugins/vuetify'
Vue.prototype.$axios = axios


axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? `https://jojoq.kz/api` : `http://caapl.local/api`;


import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);
Vue.router = router;

import VueAuth from '@websanova/vue-auth/src';

Vue.use(VueAuth, {
    auth: {
        request(req, token) {
            this.options.http._setHeaders.call(this, req, {
                Authorization: 'Bearer ' + token
            });
        },
        response(res) {
            return (res.data.meta || {}).token;
        }
    },
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    loginData: {url: '/login', fetchUser: true},
    refreshData: {enabled: false},
    rolesVar: 'role',
    fetchData: {url: '/me'},
    authRedirect: {path: '/admin/login'},
    notFoundRedirect: {path: '/admin/forbidden'}
});

Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    router,
    vuetify: Vuetify,
    components: { App }
});
