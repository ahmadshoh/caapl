import VueRouter from "vue-router";

import Index from '../components/index';
import Rights from '../components/rights';
import Messages from '../components/messages';

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/',
            component: Index
        },
        {
            path: '/rights',
            component: Rights
        },

        {
            path: '/messages',
            component: Messages
        }
    ]
});