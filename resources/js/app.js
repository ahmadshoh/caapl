// require('./bootstrap');

import Vue from 'vue';
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
import App from './components/App.vue';

// import Loading from 'vue-loading-overlay';

import VueRouter from 'vue-router';


Vue.use(VueRouter);
Vue.use(VueSweetalert2);
// Vue.use(Loading);

Vue.prototype.$axios = axios

import 'sweetalert2/dist/sweetalert2.min.css';

import VueMeta from 'vue-meta'
Vue.use(VueMeta)

import router from './router'

const app = new Vue({
    el: '#app',
    router,
    components: { App }
});
