<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;500;600;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>“АМЕС” — Алматинская модель по контролю за эпидемией ВИЧ</title>
</head>

<body>
    <div id="app">
        <app></app>
    </div>

     {{-- <script>
        const radios = document.querySelectorAll("input[type='radio']");
        const regex = /\sactive/;   

        radios.forEach((radio) => {
            radio.onchange = (e) => {
                const { target } = e;
                const { parentElement: { parentElement }} = target;
                const extendedInput = parentElement.querySelector(".extended__input");
                    
                if (target.className === "special") {
                    const { className } = extendedInput;

                    extendedInput.className = `${className} active`
                } else {
                    if (extendedInput) {
                        const { className } = extendedInput;

                        extendedInput.className = className.replace(regex, "");
                    }
                }
            }
        })
    </script> --}}


    <script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>


<script>
    $(document).ready(function() {
        $("a.scroll_to").click(function (event) {
            event.preventDefault();
            var elementClick = $(this).attr("href");
            var destination = $(elementClick).offset().top;
            $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
        });
        $('#reportmenu>ul li a').click(function() {
            $('#reportmenu .submenu').toggleClass('active');
        });
    });
</script>

     <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>