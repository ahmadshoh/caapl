<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = \HttpOz\Roles\Models\Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Custodians of the system.', // optional
            'group' => 'default' // optional, set as 'default' by default
        ]);
        
        $moderatorRole = \HttpOz\Roles\Models\Role::create([
            'name' => 'Moderator',
            'slug' => 'moderator',
        ]);

        $userRole = \HttpOz\Roles\Models\Role::create([
            'name' => 'User',
            'slug' => 'user',
        ]);
    }
}
