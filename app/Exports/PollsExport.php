<?php

namespace App\Exports;

use App\Models\Poll;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PollsExport implements FromView
{
    public function view(): View
    {
        return view('exports.polls', [
            'polls' => Poll::all()
        ]);
    }
}