<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Message;
use App\Models\Poll;


use App\Http\Resources\PollsResource;
use App\Exports\PollsExport;

use Excel;


class ApiController extends Controller
{
    public function getData() {
        $messagesCount = Message::all()->count();
        $pollsCount = Poll::all()->count();
        $repliedMessagesCount = Message::whereNotNull('answer')->count();

        return response()->json([
            'messagesCount' => $messagesCount,
            'pollsCount' => $pollsCount,
            'repliedMessagesCount' => $repliedMessagesCount,
        ]);
    }

    public function exportPolls() {
        return Excel::download(new PollsExport, 'polls.xlsx');
    }
}
