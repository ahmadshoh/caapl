<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Poll;
use App\Http\Resources\PollsResource;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polls = Poll::all();
        return response()->json(PollsResource::collection($polls));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->except(['_token', 'query_string', 'question5Special', 'question10Special', 'question11Special', 'question12Special']) as $data => $value) {
            $valids[$data] = "required";
        }
    
        $validator = validator($request->all(), $valids);

        if ($validator->fails()) {
            foreach ($validator->messages()->getMessages() as $key => $val) {
                $response["$key"] = $val;
            }
            return response()->json($response, 400);
        }

        $params = $request->all();

        $params['question5'] = $request->question5 === "special" ? $request->question5Special : $request->question5;
        $params['question10'] = $request->question10 == 'special' ? $request->question10Special : $request->question10;
        $params['question11'] = $request->question11 == 'special' ? $request->question11Special : $request->question11;
        $params['question12'] = $request->question12 == 'special' ? $request->question12Special : $request->question12;

        try {
            $poll = Poll::create($params);

            return response()->json([
                'message' => 'Данные успешно сохранены!',
                'poll' => $poll
            ], 200);
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
