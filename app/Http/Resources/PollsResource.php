<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class PollsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->created_at = Carbon::parse($this->created_at)->format('d-m-Y H:i:s');

        return [
            'id' => $this->id,
            'question1' => $this->question1,
            'question2' => $this->question2,
            'question3' => $this->question3,
            'question4' => $this->question4,
            'question5' => $this->question5,
            'question6' => $this->question6,
            'question7' => $this->question7,
            'question8' => $this->question8,
            'question9' => $this->question9,
            'question10' => $this->question10,
            'question11' => $this->question11,
            'question12' => $this->question12,
            'question13' => $this->question13,
            'question14' => $this->question14,
            'question15' => $this->question15,
            'question16' => $this->question16,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
